package buissiness;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import dao.ClientDAO;
import model.Client;
public class ClientProcessing {
	private ClientDAO c;
	
	public ClientProcessing(){
		c=new ClientDAO();
	}
	public Client selectClientByName(String name){
		List<Client> cl=c.selectQuery("name", name);
		if(cl.size()==0){
			System.out.println("\nyo\n");
			return null;}
		return cl.get(0);
	}
	public List<Client> selectAll(){
		List<Client> cl=c.selectAllQuery();
		return cl;
	}
	
	public JTable getJTable(){
		Class clas=Client.class;
		String[] head=new String[clas.getDeclaredFields().length];
		int i=0;
		for(Field field:clas.getDeclaredFields())
		{
			head[i]=field.getName();
			i++;
		}
		JTable ret;
		if(c.createData()==null)
			System.out.println("its shit\n");
		ret=new JTable(c.createData(),head);
		return ret;
	}
	
	public int deleteClient(String name){
		return c.deleteQuery("name", name);
		
	}
	
	public int insertClient(Client client){
		if(c.insertQuery(client, "wharehouse")==null)
			return 0;
		else return 1;
			
	}
	
	public int updateClient(String name,Client client)
	{
		List<Field> changeField=new ArrayList<Field>();
		Class type = Client.class;
		for (Field field : type.getDeclaredFields())
			changeField.add(field);
		List<String> changeValue=new ArrayList<String>();
		changeValue.add(client.getName());
		changeValue.add(Integer.toString(client.getPhoneNumber()));
		
		return c.updateQuerry(changeField, changeValue, "name", name, "wharehouse");
	}
	
}
