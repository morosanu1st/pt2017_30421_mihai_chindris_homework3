package buissiness;

import java.util.List;

import dao.OrderDAO;
import dao.ProductCountDAO;
import model.Order;
import model.ProductCount;


public class OrderProcessing {
	private OrderDAO o;
	private ProductCountDAO oc;
	
	public OrderProcessing(){
		o=new OrderDAO();
		oc=new ProductCountDAO();
	}
	
	public Order selectOrderById(int id){
		List<Order> or=o.selectQuery("orderId", Integer.toString(id));
		if(or.size()==0)
			return null;
		return or.get(0);
	}
	public List<Order> selectAll(){
		List<Order> or=o.selectAllQuery();
		if(or==null)
			return null;
		return or;
	}
	
	public int deleteOrder(String name){
		return o.deleteQuery("name", name);
		
	}
	
	public int createOrder(Order order){
		if(o.insertQuery(order, "wharehouse")==null)
			return 0;
		else return order.getOrderID();
	}
	
	public int addProduct(ProductCount pr){
		if(oc.insertQuery(pr, "wharehouse")==null)
			return 0;
		else return 1;
	}
	
	public Integer getIdCount(){
		int id=1;
		while(selectOrderById(id)!=null)
			id++;
		return id;
	}
}
