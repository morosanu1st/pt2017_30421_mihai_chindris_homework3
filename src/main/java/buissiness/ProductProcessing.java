package buissiness;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import dao.ProductDAO;
import model.Product;
public class ProductProcessing {
	ProductDAO dao=new ProductDAO();
	public int addProduct(String name,String color,int price,int stock){
		Product p=new Product();
		p.setProductName(name);
		p.setProductColor(color);
		p.setPrice(price);
		p.setStock(stock);
		if (dao.insertQuery(p, "wharehouse")==null){
				System.out.println("couldnt insert product "+p.toString());
				return 0;
		}
		return 1;	
	}
	
	public Product selectproductByName(String name){
		List<Product> pr=dao.selectQuery("productName", name);
		if(pr.size()==0)
			return null;
		return pr.get(0);
	}
	public List<Product> selectAll(){
		List<Product> pr=dao.selectAllQuery();
		if(pr==null)
			return null;
		return pr;
	}
	
	public JTable getJTable(){
		Class clas=Product.class;
		String[] head=new String[clas.getDeclaredFields().length];
		int i=0;
		for(Field field:clas.getDeclaredFields())
		{
			head[i]=field.getName();
			i++;
		}
		JTable ret;
		if(dao.createData()==null)
			System.out.println("its shit\n");
		ret=new JTable(dao.createData(),head);
		return ret;
	}
	public JTable getUnderstockTable(){
		Class clas=Product.class;
		String[] head=new String[clas.getDeclaredFields().length];
		int i=0;
		for(Field field:clas.getDeclaredFields())
		{
			head[i]=field.getName();
			i++;
		}
		JTable ret;
		if(dao.createData()==null)
			System.out.println("its shit\n");
		ret=new JTable(dao.createSomeData(),head);
		return ret;
	}
	
	public int tryDecrement(int count,String name)
	{
		if(dao.selectQuery("productName", name)==null)
			return -2;
		Product prod=dao.selectQuery("productName", name).get(0);
		if(prod.getStock()<count)
			return -1;
		
		prod.setProductName("");
		prod.setProductColor("");
		prod.setPrice(prod.getPrice());
		prod.setStock(prod.getStock()-count);
		if(updateproduct(name,prod)==0)
			return -3;
		return 1;
	}
	
	public List<Product> getUnderstockedProducts(int count){
		List<Product> pr=dao.selectQuery("stock<", Integer.toString(count));
		if(pr.size()==0)
			return null;
		return pr;
	}
	
	public int deleteproduct(String name){
		return dao.deleteQuery("productName", name);
		
	}
	public int updateproduct(String name,Product product)
	{
		List<Field> changeField=new ArrayList<Field>();
		Class type = Product.class;
		for (Field field : type.getDeclaredFields())
			changeField.add(field);
		List<String> changeValue=new ArrayList<String>();
		changeValue.add(product.getProductName());
		changeValue.add(product.getProductColor());
		changeValue.add(Integer.toString(product.getPrice()));
		changeValue.add(Integer.toString(product.getStock()));
		return dao.updateQuerry(changeField, changeValue, "productName", name, "wharehouse");
	}
}
