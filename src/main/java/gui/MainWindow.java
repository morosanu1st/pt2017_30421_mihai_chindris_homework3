package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainWindow {

	private JFrame frame;
	private ClientsWindow clientW;
	private ProductsWindow productW;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 241, 230);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton clients = new JButton("Manage Clients");
		clients.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				clientW=new ClientsWindow();
				clientW.setVisible(true);	
			}
		
		});
		clients.setBounds(50, 38, 135, 23);
		frame.getContentPane().add(clients);
		
		JButton products = new JButton("Manage Products");
		products.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				productW=new ProductsWindow();
				productW.setVisible(true);	}
			});
		products.setBounds(50, 84, 135, 23);
		frame.getContentPane().add(products);
		
		
	}
}
