package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import buissiness.ProductProcessing;
import model.Product;

public class ProductsWindow extends JFrame {

	private JPanel contentPane;
	private ProductProcessing p=new ProductProcessing();
	
	public ProductsWindow() {
		setTitle("Manage products");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 249, 230);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAdd = new JButton("Add Product");
		btnAdd.setBounds(56, 11, 130, 23);
		btnAdd.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				AddWindow f=new AddWindow();
				f.setVisible(true);
			}});
		contentPane.add(btnAdd);
		
		JButton btnDelete = new JButton("Delete Product");
		btnDelete.setBounds(56, 45, 130, 23);
		btnDelete.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				DeleteWindow f=new DeleteWindow();
				f.setVisible(true);
			}});
		contentPane.add(btnDelete);
		
		JButton btnUpdate = new JButton("Update Product");
		btnUpdate.setBounds(56, 79, 130, 23);
		btnUpdate.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				UpdateWindow f=new UpdateWindow();
				f.setVisible(true);
			}});
		contentPane.add(btnUpdate);
		
		JButton btnListAllProducts = new JButton("List All Products");
		btnListAllProducts.setBounds(56, 113, 130, 23);
		btnListAllProducts.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				ListAllWindow f=new ListAllWindow();
				f.setVisible(true);
			}});
		contentPane.add(btnListAllProducts);
		
		JButton btnListUnderstockProducts = new JButton("Understock report");
		btnListUnderstockProducts.setBounds(46, 143, 150, 23);
		btnListUnderstockProducts.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				ListUnderstockWindow f=new ListUnderstockWindow();
				f.setVisible(true);
			}});
		contentPane.add(btnListUnderstockProducts);
	}
	
	private class AddWindow extends JFrame {

		private JPanel contentPane;
		private JTextField name;
		private JTextField color;
		private JTextField price;
		private JTextField stock;
		private JLabel warn=new JLabel("");
		
		public AddWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 268, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			warn.setBounds(10,205, 250, 20);
			warn.setVisible(true);
			contentPane.add(warn);
			
			name = new JTextField();
			name.setBounds(10, 23, 123, 20);
			contentPane.add(name);
			name.setColumns(10);
			
			color = new JTextField();
			color.setBounds(10, 77, 123, 20);
			contentPane.add(color);
			color.setColumns(10);
			
			price = new JTextField();
			price.setBounds(10, 127, 123, 20);
			contentPane.add(price);
			price.setColumns(10);
			
			stock = new JTextField();
			stock.setBounds(10, 184, 123, 20);
			contentPane.add(stock);
			stock.setColumns(10);
			
			JButton btnCreate = new JButton("Create");
			btnCreate.setBounds(63, 228, 89, 23);
			btnCreate.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					warn.setText("");
					int clear=1;
					Product prod=new Product();
					if(name.getText()!=""&&name.getText().length()<45)
						if(p.selectproductByName(name.getText())!=null)
						{
							warn.setText("this product already exists");
							clear=0;
						}
						else
							prod.setProductName(name.getText());
					else
					{
						warn.setText("please insert a propper name");
						clear=0;
					}
					String temp=color.getText();
					if(temp!=""&&temp.length()<45)
						prod.setProductColor(temp);
					else
					{
						warn.setText("please insert a propper color");
						clear=0;
					}
					temp=price.getText();
					int num=0;
					try {
						num=Integer.parseInt(temp);
					} catch (NumberFormatException e1) {
						warn.setText("please insert a propper price");
						clear=0;
						//e1.printStackTrace();
					}finally{
						prod.setPrice(num);
					}
					temp=stock.getText();
					try {
						num=Integer.parseInt(temp);
					} catch (NumberFormatException e1) {
						warn.setText("please insert a propper stock");
						clear=0;
						//e1.printStackTrace();
					}finally{
						prod.setStock(num);
					}
					
					if(clear==1)
					{
						if(p.addProduct(prod.getProductName(), prod.getProductColor(), prod.getPrice(), prod.getStock())==1)
							warn.setText("product succesfully added");
						else
							warn.setText("product could not be added");
					}
					
					
				}});
			contentPane.add(btnCreate);
			
			JLabel lblName = new JLabel("Name");
			lblName.setBounds(10, 0, 46, 14);
			contentPane.add(lblName);
			
			JLabel lblColor = new JLabel("Color");
			lblColor.setBounds(10, 54, 46, 14);
			contentPane.add(lblColor);
			
			JLabel lblPrice = new JLabel("Price");
			lblPrice.setBounds(10, 102, 46, 14);
			contentPane.add(lblPrice);
			
			JLabel lblStock = new JLabel("Stock");
			lblStock.setBounds(10, 159, 46, 14);
			contentPane.add(lblStock);
		}

	}
	class DeleteWindow extends JFrame {

		private JPanel contentPane;
		private JTextField name;
		JLabel warn = new JLabel("");
		
		public DeleteWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 228, 257);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			warn = new JLabel("");
			warn.setBounds(40, 88, 166, 14);
			contentPane.add(warn);
			
			name= new JTextField();
			name.setBounds(60, 40, 86, 20);
			contentPane.add(name);
			name.setColumns(10);
			
			JButton btnNewButton = new JButton("Delete");
			btnNewButton.setBounds(60, 113, 89, 23);
			btnNewButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					warn.setText("");
					if(p.selectproductByName(name.getText())!=null)
						if(p.deleteproduct(name.getText())==1)
							warn.setText("product deleted succesfully");
						else
							warn.setText("the product could not be deleted");
					else
						warn.setText("there is no such product");
				}});
			contentPane.add(btnNewButton);
			
			JLabel lblProductName = new JLabel("Product name");
			lblProductName.setBounds(60, 15, 106, 14);
			contentPane.add(lblProductName);
			
			
		}

	}
	private class UpdateWindow extends JFrame {

		private JPanel contentPane;
		private JTextField oldName;
		private JTextField name;
		private JTextField color;
		private JTextField price;
		private JTextField stock;
		JLabel warn = new JLabel("");
		

		
		public UpdateWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 360, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			warn.setBounds(145, 79, 200, 14);
			contentPane.add(warn);
			
			oldName = new JTextField();
			oldName.setBounds(10, 28, 86, 20);
			contentPane.add(oldName);
			oldName.setColumns(10);
			
			name = new JTextField();
			name.setBounds(10, 76, 86, 20);
			contentPane.add(name);
			name.setColumns(10);
			
			color = new JTextField();
			color.setBounds(10, 122, 86, 20);
			contentPane.add(color);
			color.setColumns(10);
			
			price = new JTextField();
			price.setBounds(10, 172, 86, 20);
			contentPane.add(price);
			price.setColumns(10);
			
			stock = new JTextField();
			stock.setBounds(10, 218, 86, 20);
			contentPane.add(stock);
			stock.setColumns(10);
			
			JButton btnUpdate = new JButton("Update");
			btnUpdate.setBounds(145, 101, 89, 23);
			btnUpdate.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				warn.setText("");
				int clear=1;
				String olnm=oldName.getText();
				if(olnm==""||olnm.length()>=45){
					warn.setText("please insert avalid product");clear=0;}
				else
					if(p.selectproductByName(olnm)==null)
					{
						warn.setText("this product does not exist");
						clear=0;
					}
				Product prod=new Product();
				String temp=name.getText();
				if(temp.length()>=45)
				{
					warn.setText("please insert a valid new name");
					clear=0;
				}
				else if(p.selectproductByName(temp)!=null)
				{
					warn.setText("this name is taken");
					clear=0;
				}
				else
					prod.setProductName(temp);
				temp=color.getText();
				if(temp.length()>=45)
				{
					warn.setText("please insert a valid color");
					clear=0;
				}
				else
					prod.setProductColor(temp);
				temp=price.getText();
				int t2;
				
					try {
						t2=Integer.parseInt(temp);
						prod.setPrice(t2);
					} catch (NumberFormatException e1) {
						warn.setText("please insert a valid price");
						clear=0;
						//e1.printStackTrace();
					}
				
				temp=stock.getText();
				
					try {
						t2=Integer.parseInt(temp);
						prod.setStock(t2);
					} catch (NumberFormatException e1) {
						warn.setText("please insert a valid stock");
						clear=0;
						//e1.printStackTrace();
					}
				
				if(clear==1)
				{
					if(p.updateproduct(olnm, prod)==1)
						warn.setText("product updated succesfully");
					else
						warn.setText("the product could not be added");
				}
				
				}});
			contentPane.add(btnUpdate);
			
			JLabel lblProductName = new JLabel("Product name");
			lblProductName.setBounds(10, 3, 86, 14);
			contentPane.add(lblProductName);
			
			JLabel lblNewName = new JLabel("New name");
			lblNewName.setBounds(10, 59, 87, 14);
			contentPane.add(lblNewName);
			
			JLabel lblNewLabel = new JLabel("New color");
			lblNewLabel.setBounds(10, 105, 86, 14);
			contentPane.add(lblNewLabel);
			
			JLabel lblNewPrice = new JLabel("New price");
			lblNewPrice.setBounds(10, 153, 86, 14);
			contentPane.add(lblNewPrice);
			
			JLabel lblNewStock = new JLabel("New stock");
			lblNewStock.setBounds(10, 193, 87, 14);
			contentPane.add(lblNewStock);
			
			
		}

	}
	private class ListAllWindow extends JFrame {

		private JPanel contentPane;
		private JTable table;
		
		public ListAllWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 11, 414, 240);
			contentPane.add(scrollPane);
			
			table = p.getJTable();
			table.setCellSelectionEnabled(false);
			table.setVisible(true);
			scrollPane.setViewportView(table);
		}
	}
	private class ListUnderstockWindow extends JFrame {

		private JPanel contentPane;
		private JTable table;
		
		public ListUnderstockWindow() {
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 11, 414, 240);
			contentPane.add(scrollPane);
			
			table = p.getUnderstockTable();
			table.setCellSelectionEnabled(false);
			table.setVisible(true);
			scrollPane.setViewportView(table);
		}
	}
}
