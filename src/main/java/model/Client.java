package model;

public class Client {
	private String name;
	private int phoneNumber;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isClient(Object obj){
		return true;
	}
	@Override
	public String toString() {
		return " (name , phoneNumber) VALUES ('"+name + "','"  + phoneNumber +"')";
	}
	
}
