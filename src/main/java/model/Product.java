package model;

public class Product {
	private String productName;
	private String productColor;
	private Integer price;
	private Integer stock;
	
	
	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductColor() {
		return productColor;
	}


	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}


	public Integer getPrice() {
		return price;
	}


	public void setPrice(Integer price) {
		this.price = price;
	}


	public Integer getStock() {
		return stock;
	}


	public void setStock(Integer stock) {
		this.stock = stock;
	}


	@Override
	public String toString() {
		return "( productName , productColor , price , stock ) VALUES ('" + productName + "','" + productColor + "','" + price
				+ "','" + stock + "')";
	}
	
	
}
